//
//  ViewController.swift
//  Times Tables App
//
//  Created by Wylie, Louise on 11/12/2020.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var input: UITextField!
    @IBOutlet weak var tableViewController: UITableView!
    var tableData = [String]()
    var inputText = ""
    var multiple = 0
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = 0
        if multiple == 40 {
            rows = 40
        } else if multiple == 100 {
            rows = 100
        }
        return rows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
        let inputInt = Int(inputText)
        var sum = ""
        if inputText != "" {
            sum = String(inputInt!*(indexPath.row+1))
            tableView.isHidden = false
        } else {
            tableView.isHidden = true
        }
        cell.textLabel!.text = "\(indexPath.row+1) x \(inputText) = \(sum)"

        return cell
    }

    @IBAction func calculateButton(_ sender: Any) {
        multiple = 40
        input.resignFirstResponder()
        inputText = input.text!
        tableViewController.reloadData()
        input.text! = ""
    }
        
    @IBAction func multiply100(_ sender: Any) {
        multiple = 100
        input.resignFirstResponder()
        inputText = input.text!
        tableViewController.reloadData()
        input.text! = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}

